import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.event.*;
import java.text.*;

public class StudentAssessment extends Object
	{
	String studentName;
	String classID;
	String assessmentID;
	int assessmentType;
	static final String[] assessmentTypeName = {"Homework","Quiz","Exam","Project"};
	Date assignedDate;
	Date submitDate;
	Date recordedDate;
	double assessmentValue;
	boolean finalSubmission;
	String comments;
	static final int HOMEWORK = 0;
	static final int QUIZ = 1;
	static final int EXAM = 2;
	static final int PROJECT = 3;

//==================================================================================
//Creates a default student assessment
	StudentAssessment()
		{
		studentName = "";
		classID = "Comp 2200";
		assessmentID = "";
		assessmentType = 0;
		assignedDate = new Date(System.currentTimeMillis());
		submitDate = new Date(System.currentTimeMillis());
		recordedDate = new Date(System.currentTimeMillis());
		assessmentValue = 0;
		comments = "";
		}
//==================================================================================
//Loads a student assessment using a DataInputStream
	StudentAssessment(DataInputStream dis) throws IOException
		{
		studentName = dis.readUTF();
		classID = dis.readUTF();
		assessmentID = dis.readUTF();
		assessmentType = dis.readInt();
		assignedDate = new Date(dis.readLong());
		submitDate = new Date(dis.readLong());
		recordedDate = new Date(dis.readLong());
		assessmentValue = dis.readDouble();
		comments = dis.readUTF();
		}
//===================================================================================
//Stores a student assessment using a DataOutputStream
	void store(DataOutputStream dos) throws IOException
		{
		dos.writeUTF(studentName);
		dos.writeUTF(classID);
		dos.writeUTF(assessmentID);
		dos.writeInt(assessmentType);
		dos.writeLong(assignedDate.getTime());
		dos.writeLong(submitDate.getTime());
		dos.writeLong(recordedDate.getTime());
		dos.writeDouble(assessmentValue);
		dos.writeUTF(comments);
		}
//===================================================================================
//Converts the information of the class to a formatted string
	@Override
	public String toString()
		{
		String space = "         ";
		return studentName + space + classID + space + assessmentID + space + assessmentTypeName[assessmentType] + space + assignedDate + space + submitDate + space + recordedDate + space + assessmentValue + space + finalSubmission + space + comments;
		}
//==================================================================================
//Returns the value of a specified field in the assessment
	String getField(int fieldIndex)
		{
		SimpleDateFormat dateFormat;
		Date currentDate;
		String dateOutput;
		dateFormat = new SimpleDateFormat("MM/dd/yy");
		if(fieldIndex == 0)
			return studentName;
		else if(fieldIndex == 1)
			return classID;
		else if(fieldIndex == 2)
			return assessmentID;
		else if(fieldIndex == 3)
			return "" + assessmentType;
		else if(fieldIndex == 4)
			{
			currentDate = new Date(assignedDate.getTime());
			dateOutput = dateFormat.format(currentDate);
			return dateOutput;
			}
		else if(fieldIndex == 5)
			{
			currentDate = new Date(submitDate.getTime());
			dateOutput = dateFormat.format(currentDate);
			return dateOutput;
			}
		else if(fieldIndex == 6)
			{
			currentDate = new Date(recordedDate.getTime());
			dateOutput = dateFormat.format(currentDate);
			return dateOutput;
			}
		else if(fieldIndex == 7)
			return "" + assessmentValue;
		else if(fieldIndex == 8)
			return "" + finalSubmission;
		else if( fieldIndex == 9)
			return comments;
		else
			return null;
		}
	}