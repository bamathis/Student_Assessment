import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.table.*;
import java.io.*;

public class AssessmentListModel  extends DefaultListModel <StudentAssessment>
	{

//======================================================================
	AssessmentListModel()
		{

		}
//======================================================================
//Constructs the assessment list using a DataInputStream
	AssessmentListModel(DataInputStream dis) throws IOException
		{
		int numElements;
		numElements = dis.readInt();
		for (int n = 0; n < numElements ; n++)
			{
			addElement(new StudentAssessment(dis));
			}
		}
	}
