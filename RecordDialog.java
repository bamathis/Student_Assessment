import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.util.*;
import java.text.*;

public class RecordDialog extends JDialog implements ActionListener
    {
    JButton saveButton;
    JButton cancelButton;
    JButton continueButton;
    JPanel savePanel;
    JPanel studentNamePanel;
    JPanel classIDPanel;
    JPanel assessmentIDPanel;
    JPanel buttonPanel;
    JPanel assignedDatePanel;
    JPanel submitDatePanel;
    JPanel recordDatePanel;
    JPanel assessmentValuePanel;
    JPanel finalSubmissionPanel;
    JPanel commentPanel;
    JLabel studentNameLabel;
    JLabel classIDLabel;
    JLabel assessmentIDLabel;
    JLabel buttonPanelLabel;
    JLabel assignedDateLabel;
    JLabel submitDateLabel;
    JLabel recordDateLabel;
    JLabel assessmentValueLabel;
    JLabel finalSubmissionLabel;
    JLabel commentLabel;
    JTextField studentNameField;
    JComboBox classIDBox;
    JTextField assessmentIDField;
    JRadioButton homeworkButton;
    JRadioButton quizButton;
    JRadioButton examButton;
    JRadioButton projectButton;
    ButtonGroup radioButtonGroup;
    JTextField assignedDateField;
    JTextField submitDateField;
    JTextField recordDateField;
    JTextField assessmentValueField;
    JCheckBox finalSubmissionBox;
    JTextArea commentsArea;
    DataManager localManager;
    StudentAssessment localAssessment;
    int localIndex;
    String errorMessage[];
    StudentAssessment tempAssessment;
    SimpleDateFormat dateFormat;
    int numErrors;
    static final String COMBO_BOX_OPTIONS[] = {"Comp 2200","Comp 2270", "Math 1190","Math 2216"};

//=============================================================================================
//Used for creating a new assessment
    RecordDialog(DataManager passedManager)
        {
        localManager = passedManager;
        addComponents();
        saveButton = new JButton("Save and exit");
        saveButton.setActionCommand("Add_Save_Exit");
        cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        continueButton = new JButton("Save and Continue");
        continueButton.setActionCommand("Save_Continue");
        continueButton.addActionListener(this);
        saveButton.addActionListener(this);
        cancelButton.addActionListener(this);
        cancelButton.setVerifyInputWhenFocusTarget(false);
        savePanel.add(saveButton);
        savePanel.add(continueButton);
        savePanel.add(cancelButton);
        add(savePanel);
        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setVisible(true);
        }
//=============================================================================================
//Used for editing an existing assessment
    RecordDialog(DataManager passedManager, StudentAssessment passedAssessment, int index)
        {
        localIndex = index;
        localAssessment = passedAssessment;
        localManager = passedManager;
        addComponents();
        setFields();
        saveButton = new JButton("Save");
        saveButton.setActionCommand("Edit_Save_Exit");
        cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        saveButton.addActionListener(this);
        cancelButton.addActionListener(this);
        cancelButton.setVerifyInputWhenFocusTarget(false);
        savePanel.add(saveButton);
        savePanel.add(cancelButton);
        add(savePanel);
        repaint();
        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setVisible(true);
        }
//=============================================================================================
//Adds the components to the JDialog
    void addComponents()
        {
        Toolkit  tk;
        Dimension d;
        GridLayout gridLayout;
        createPanels();
        createLabels();
        createFields();
        gridLayout = new GridLayout(12,1);
        setLayout(gridLayout);
        tk = Toolkit.getDefaultToolkit();
        d = tk.getScreenSize();
        setSize(d.width/3, d.height/2 + 30);
        setMinimumSize(new Dimension(d.width/3, d.height/2 + 30));
        setLocation(d.width/4,d.height/4);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Record");
        studentNamePanel.add(studentNameLabel);
        studentNamePanel.add(studentNameField);
        classIDPanel.add(classIDLabel);
        classIDPanel.add(classIDBox);
        assessmentIDPanel.add(assessmentIDLabel);
        assessmentIDPanel.add(assessmentIDField);
        buttonPanel.add(buttonPanelLabel);
        buttonPanel.add(homeworkButton);
        buttonPanel.add(quizButton);
        buttonPanel.add(examButton);
        buttonPanel.add(projectButton);
        assignedDatePanel.add(assignedDateLabel);
        assignedDatePanel.add(assignedDateField);
        submitDatePanel.add(submitDateLabel);
        submitDatePanel.add(submitDateField);
        recordDatePanel.add(recordDateLabel);
        recordDatePanel.add(recordDateField);
        assessmentValuePanel.add(assessmentValueLabel);
        assessmentValuePanel.add(assessmentValueField);
        finalSubmissionPanel.add(finalSubmissionLabel);
        finalSubmissionPanel.add(finalSubmissionBox);
        add(studentNamePanel);
        add(classIDPanel);
        add(assessmentIDPanel);
        add(buttonPanel);
        add(assignedDatePanel);
        add(submitDatePanel);
        add(recordDatePanel);
        add(assessmentValuePanel);
        add(finalSubmissionPanel);
        add(commentLabel);
        add(commentsArea);
        }
//=============================================================================================
//Creates the labels for the JDialog
        void createLabels()
            {
            studentNameLabel = new JLabel("Student Name: ");
            classIDLabel = new JLabel("Class ID: ");
            assessmentIDLabel = new JLabel("Assessment ID: ");
            buttonPanelLabel = new JLabel("Assessment Type: ");
            assignedDateLabel = new JLabel("Assigned Date(MM/DD/YY): ");
            submitDateLabel = new JLabel("Submit Date(MM/DD/YY): ");
            recordDateLabel = new JLabel("Record Date: ");
            assessmentValueLabel = new JLabel("Assessment Value: ");
            finalSubmissionLabel = new JLabel("Final Submission? ");
            commentLabel = new JLabel("Comments: ");
            }
//===============================================================================================
//Creates the panels for the JDialog
        void createPanels()
            {
            studentNamePanel = new JPanel();
            classIDPanel = new JPanel();
            assessmentIDPanel = new JPanel();
            buttonPanel = new JPanel();
            assignedDatePanel = new JPanel();
            submitDatePanel = new JPanel();
            recordDatePanel = new JPanel();
            assessmentValuePanel = new JPanel();
            finalSubmissionPanel = new JPanel();
            commentPanel = new JPanel();
            savePanel = new JPanel();
            }
//====================================================================================================
//Creates the input fields for the JDialog
        void createFields()
            {
            String dateOutput;
            Date currentDate;
            AssessmentValueVerifier assessmentVerifier;
            DateVerifier dateVerifier;
            try
                {
                dateFormat = new SimpleDateFormat("MM/dd/yy");
                assessmentVerifier = new AssessmentValueVerifier();
                dateVerifier = new DateVerifier();
                studentNameField = new JTextField(25);
                classIDBox = new JComboBox(COMBO_BOX_OPTIONS);
                assessmentIDField = new JTextField(25);
                homeworkButton = new JRadioButton("Homework");
                quizButton = new JRadioButton("Quiz");
                examButton = new JRadioButton("Exam");
                projectButton = new JRadioButton("Project");
                radioButtonGroup = new ButtonGroup();
                assignedDateField = new JTextField(20);
                assignedDateField.setInputVerifier(dateVerifier);
                submitDateField = new JTextField(20);
                submitDateField.setInputVerifier(dateVerifier);
                recordDateField = new JTextField(25);
                recordDateField.setInputVerifier(dateVerifier);
                assessmentValueField = new JTextField(25);
                assessmentValueField.setInputVerifier(assessmentVerifier);
                finalSubmissionBox = new JCheckBox();
                commentsArea = new JTextArea(20,20);
                radioButtonGroup.add(homeworkButton);
                radioButtonGroup.add(quizButton);
                radioButtonGroup.add(examButton);
                radioButtonGroup.add(projectButton);
                buttonPanel.add(homeworkButton);
                buttonPanel.add(quizButton);
                buttonPanel.add(examButton);
                buttonPanel.add(projectButton);
                currentDate = new Date(System.currentTimeMillis());
                dateOutput = dateFormat.format(currentDate);
                recordDateField.setText(dateOutput);
                recordDateField.setEnabled(false);
                }
            catch(Exception e)
                {
                System.out.println("Caught exception in createFields");
                }
            }
//===================================================================================
//Sets the default values for the input fields of the JDialog
    void setFields()
        {
        String dateOutput;
        Date currentDate;
        int radioButtonIndex;
        int IDBoxIndex;
        studentNameField.setText(localAssessment.getField(0));
        if(localAssessment.getField(1) == "Comp 2200")
            IDBoxIndex = 0;
        else if(localAssessment.getField(1) == "Comp 2270")
            IDBoxIndex = 1;
        else if(localAssessment.getField(1) == "Math 1190")
            IDBoxIndex = 2;
        else
            IDBoxIndex = 3;
        classIDBox.setSelectedIndex(IDBoxIndex);
        assessmentIDField.setText(localAssessment.getField(2));
        radioButtonIndex = Integer.parseInt(localAssessment.getField(3));
        if(radioButtonIndex == 0)
            homeworkButton.setSelected(true);
        else if(radioButtonIndex == 1)
            quizButton.setSelected(true);
        else if(radioButtonIndex == 2)
            examButton.setSelected(true);
        else if(radioButtonIndex == 3)
            projectButton.setSelected(true);
        assignedDateField.setText(localAssessment.getField(4));
        submitDateField.setText(localAssessment.getField(5));
        recordDateField.setText(localAssessment.getField(6));
        assessmentValueField.setText(localAssessment.getField(7));
        finalSubmissionBox.setSelected(Boolean.parseBoolean(localAssessment.getField(8)));
        commentsArea.setText(localAssessment.getField(9));
        }
//====================================================================================
//Handles user interaction with the buttons
    public void actionPerformed(ActionEvent e)
        {
        if(e.getActionCommand().equals("Cancel"))
            dispose();
        else if(e.getActionCommand().equals("Add_Save_Exit"))
            newSaveAndExit();
        else if(e.getActionCommand().equals("Save_Continue"))
            saveAndContinue();
        else if(e.getActionCommand().equals("Edit_Save_Exit"))
            editSaveAndExit();
        }
//======================================================================================
//Adds the record to the assessment model and closes the JDialog
    void newSaveAndExit()
        {
        numErrors = 0;
        checkValidFields();
        if (numErrors == 0)
            {
            localManager.addRecord(tempAssessment);
            dispose();
            }
        else
            {
            String errorString;
            errorString = "Error required: ";
            for(int n = 0; n < numErrors - 1; n++)
                {
                errorString += errorMessage[n];
                errorString += ", ";
                }
            errorString += errorMessage[numErrors - 1];
            JOptionPane.showMessageDialog(this, errorString);
            }
        }
//=======================================================================================
//Adds the record to the assessment model and clears the values of the JDialog
    void saveAndContinue()
        {
        Date currentDate;
        String dateOutput;
        numErrors = 0;
        checkValidFields();
        if (numErrors == 0)
            {
            localManager.addRecord(tempAssessment);
            studentNameField.setText("");
            classIDBox.setSelectedIndex(0);
            assessmentIDField.setText("");
            assignedDateField.setText("");
            submitDateField.setText("");
            radioButtonGroup.clearSelection();
            currentDate = new Date(System.currentTimeMillis());
            dateOutput = dateFormat.format(currentDate);
            recordDateField.setText(dateOutput);
            assessmentValueField.setText("");
            finalSubmissionBox.setSelected(false);
            commentsArea.setText("");
            studentNameField.requestFocus();
            }
        else
            {
            String errorString;
            errorString = "Error required: ";
            for(int n = 0; n < numErrors - 1; n++)
                {
                errorString += errorMessage[n];
                errorString += ", ";
                }
            errorString += errorMessage[numErrors - 1];
            JOptionPane.showMessageDialog(this, errorString);
            }
        }
//=====================================================================================
//Saves the editted record to the assessment model and closes the JDialog
    void editSaveAndExit()
        {
        numErrors = 0;
        checkValidFields();
        if (numErrors == 0)
            {
            localManager.editRecord(tempAssessment,localIndex);
            dispose();
            }
        else
            {
            String errorString;
            errorString = "Error required: ";
            for(int n = 0; n < numErrors - 1; n++)
                {
                errorString += errorMessage[n];
                errorString += ", ";
                }
           errorString += errorMessage[numErrors - 1];
           JOptionPane.showMessageDialog(this, errorString);
            }
        }
//===================================================================================
//Checks that the input fields have valid values
    void checkValidFields()
        {
        Date tempAssignedDate;
        Date tempSubmitDate;
        errorMessage = new String[9];
        tempAssessment = new StudentAssessment();
        try
            {
            if (!studentNameField.getText().trim().equals(""))
                tempAssessment.studentName = studentNameField.getText();
          	else
          	      {
          	      errorMessage[numErrors] = "student name";
          	      numErrors++;
          	      }
          	if(classIDBox.getSelectedItem() != null)
                tempAssessment.classID = (String)classIDBox.getSelectedItem();
            else
           		{
            	errorMessage[numErrors] = "class ID";
            	numErrors++;
            	}
        	if (!assessmentIDField.getText().trim().equals(""))
        	    tempAssessment.assessmentID = assessmentIDField.getText();
        	else
        	    {
        	    errorMessage[numErrors] = "assessment ID";
        	    numErrors++;
        	    }
        	if(homeworkButton.isSelected())
        	    tempAssessment.assessmentType = 0;
        	else if (quizButton.isSelected())
        	    tempAssessment.assessmentType = 1;
        	else if (examButton.isSelected())
        	    tempAssessment.assessmentType = 2;
        	else if (projectButton.isSelected())
       	    	tempAssessment.assessmentType = 3;
        	else
        	    {
        	    errorMessage[numErrors] = "assessment type";
        	    numErrors++;
        	    }
        	if (!assessmentValueField.getText().trim().equals(""))
        	    tempAssessment.assessmentValue = Double.parseDouble(assessmentValueField.getText());
        	else
        	    {
        	    errorMessage[numErrors] = "assessment value";
        	    numErrors++;
        	    }
        	tempAssessment.finalSubmission = finalSubmissionBox.isSelected();
        	tempAssignedDate = dateFormat.parse(assignedDateField.getText());
        	tempSubmitDate = dateFormat.parse(submitDateField.getText());
        	if(tempAssignedDate.getTime() <= tempSubmitDate.getTime())
        	    {
        	    tempAssessment.assignedDate = tempAssignedDate;
        	    tempAssessment.submitDate = tempSubmitDate;
            	}
        	else
        	    {
        	    errorMessage[numErrors] = "assigned date before submit date";
        	    numErrors++;
        	    }
        	if(tempAssignedDate.getTime() <= System.currentTimeMillis() && tempSubmitDate.getTime() <= System.currentTimeMillis())
        	    tempAssessment.recordedDate = new Date (System.currentTimeMillis());
        	else if(tempAssignedDate.getTime() > System.currentTimeMillis() && tempSubmitDate.getTime() > System.currentTimeMillis())
        	    {
        	    errorMessage[numErrors] = "assigned date before recorded date";
        	    numErrors++;
        	    errorMessage[numErrors] = "submit date before recorded date";
        	    numErrors++;
        	    }
        	else if(tempAssignedDate.getTime() > System.currentTimeMillis())
        	    {
        	    errorMessage[numErrors] = "assigned date before recorded date";
        	    numErrors++;
        	    }
        	else if(tempSubmitDate.getTime() > System.currentTimeMillis())
        	    {
        	    errorMessage[numErrors] = "submit date before recorded date";
        	    numErrors++;
        	    }
            }
        catch(Exception e)
            {
            errorMessage[numErrors] = "valid dates";
            numErrors++;
            }
        }
    }
