import javax.swing.*;
import java.util.*;
import java.text.*;

public class DateVerifier extends InputVerifier
    {

//===========================================================================
//Checks that the input to the JTextField was a valid date and was formatted correctly
    @Override
    public boolean verify(JComponent c)
        {
        String dateInput;
        ParsePosition position;
        Date date;
        SimpleDateFormat dateFormat;
        JTextField dateInputField;
        dateFormat = new SimpleDateFormat("MM/dd/yy");
        dateFormat.setLenient(false);
        position = new ParsePosition(0);
        dateInputField = (JTextField) c;
        dateInput = dateInputField.getText().trim();
        if (dateInput.equals(""))
            return true;
        try
            {
            date = dateFormat.parse(dateInput,position);
            if(date != null && position.getIndex() == dateInput.length())
                return true;
            else
                JOptionPane.showMessageDialog(c, "Input was not a valid date");
            }
        catch(Exception e)
            {
            JOptionPane.showMessageDialog(c, "Input was not a valid date");
            }
        return false;
        }
    }