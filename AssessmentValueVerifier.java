import javax.swing.*;
import java.util.*;
import java.text.*;

public class AssessmentValueVerifier extends InputVerifier
    {
//Checks that the input to the JTextField was a valid input between 0 and 120
    @Override
    public boolean verify(JComponent c)
        {
        double assessmentValue;
        String userInput;
        assessmentValue = 0;
        JTextField assessmentField;
        assessmentField = (JTextField)c;
        userInput = assessmentField.getText().trim();
        if(userInput.equals(""))
            return true;
        try
            {
            assessmentValue = Double.parseDouble(userInput);
            if(assessmentValue >= 0 && assessmentValue <= 120)
                return true;
            else
                JOptionPane.showMessageDialog(c, "Assessment Value has to be between 0 - 120");
            }
        catch(Exception e)
            {
            JOptionPane.showMessageDialog(c, "Input was not a valid number");
            }
        return false;
        }
    }