import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.table.*;
import java.io.*;

public class AssessmentTableModel extends AbstractTableModel implements DataManager, TableModel
    {
    AssessmentListModel assessmentModel;
    static final int NUM_COLUMNS = 9;

//==============================================================
    AssessmentTableModel()
        {
        assessmentModel = new AssessmentListModel();
        }
//==============================================================
//Constructs the assessment model using a DataInputStream
    AssessmentTableModel(DataInputStream dis) throws IOException
        {
        assessmentModel = new AssessmentListModel(dis);
        }
//===============================================================
//Adds a record to the assessment model
    public void addRecord(StudentAssessment studentAssessment)
        {
        assessmentModel.addElement(studentAssessment);
        fireTableDataChanged();
        }
//==============================================================
//Edits a record in the assessment model
    public void editRecord(StudentAssessment studentAssessment, int index)
        {
        assessmentModel.removeElementAt(index);
        assessmentModel.add(index,studentAssessment);
        fireTableDataChanged();
        }
//==============================================================
//Gets the row count of the assessment model
    @Override
    public int getRowCount()
        {
        return assessmentModel.size();
        }
//==============================================================
//Gets the column count of the assessment model
    @Override
    public int getColumnCount()
        {
        return NUM_COLUMNS;
        }
//==============================================================
//Returns the value at the given row and column
    @Override
    public Object getValueAt(int row, int column)
        {
        StudentAssessment tempAssessment;
        tempAssessment = assessmentModel.elementAt(row);
        if(column == 3)
            {
            int value = Integer.parseInt(tempAssessment.getField(column));
            return StudentAssessment.assessmentTypeName[value];
            }
        else if(column == 8)
            {
            if(Boolean.parseBoolean(tempAssessment.getField(column)))
                return "Yes";
            else
                return "No";
            }
        else
            return tempAssessment.getField(column);
        }
//==============================================================
//Saves the assessment model using a DataOutputStream
    void store(DataOutputStream dos)
        {
        try
            {
            int size = assessmentModel.size();
            dos.writeInt(size);
            for(int n = 0; n < size; n++)
                assessmentModel.elementAt(n).store(dos);
            }
        catch(Exception e)
            {
            JOptionPane.showMessageDialog(null,"Could not write to file");
            }
        }
    }
