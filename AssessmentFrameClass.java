import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.table.*;
import java.io.*;
import javax.swing.event.*;

public class AssessmentFrameClass
	{
	public static void main(String[] args)
		{
	    System.out.println("Starting application");
	    new AssessmentFrame();
	    System.out.println("Done");
		}
	}
//==========================================================================
class AssessmentFrame extends JFrame implements ActionListener, ListSelectionListener
	{
	DataOutputStream dos;
	DataInputStream dis;
	RecordDialog tempDialog;
	AssessmentTableModel assessmentTableModel;
	JTable assessmentTable;
	JButton storeButton;
	JButton loadButton;
	JButton addButton;
	JButton deleteButton;
	StudentAssessment myAssessment;
	ListSelectionModel listSelectionModel;
	int [] selectedList;
	JButton editButton;
	JPanel buttonPanel;
	JScrollPane scroller;
	DefaultTableColumnModel colModel;
	static final String[] headers = {"Student Name", "Class ID", "Assessment ID", "Assessment type", " Date assigned",
									"Date submitted", "Date recorded", "Assessment value", "Final Submission?","Comments"};

//============================================================================
	AssessmentFrame()
		{
		selectedList = new int[30];
		buttonPanel = new JPanel();
		addButton = new JButton ("Add");
		addButton.addActionListener(this);
		editButton = new JButton("Edit");
		editButton.addActionListener(this);
		deleteButton = new JButton("Delete");
		deleteButton.addActionListener(this);
		storeButton = new JButton ("Store");
		storeButton.addActionListener(this);
		loadButton = new JButton("Load");
		loadButton.addActionListener(this);
		buttonPanel.add(storeButton);
		buttonPanel.add(loadButton);
		buttonPanel.add(addButton);
		buttonPanel.add(editButton);
		buttonPanel.add(deleteButton);
		getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		assessmentTableModel = new AssessmentTableModel();
		assessmentTableModel = new AssessmentTableModel();
		assessmentTable = new JTable(assessmentTableModel);
		assessmentTable.setFont(new Font("Courier", Font.BOLD,12));
        assessmentTable.setMinimumSize(new Dimension(10, 10));
		makeColumns();
		scroller = new JScrollPane(assessmentTable);
		assessmentTable.setPreferredScrollableViewportSize(new Dimension(50,50));
		getContentPane().add(scroller,BorderLayout.NORTH);
		listSelectionModel = assessmentTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(this);
		myAssessment = new StudentAssessment();
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);
		setupMainFrame();
		}
//============================================================================
//Sets the initial values for the JFrame
	void setupMainFrame()
		{
        Toolkit  tk;
        Dimension d;
        tk = Toolkit.getDefaultToolkit();
        d = tk.getScreenSize();
        setSize(d.width/2 + 30, d.height/2);
        setMinimumSize(new Dimension(d.width/3 , d.height/3));
        setLocation(d.width/4,d.height/4);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Student Assessment");
        setVisible(true);
		}
//=============================================================================
//Creates the columns for the JTable
	void makeColumns()
		{
		TableColumn col;
		colModel = new DefaultTableColumnModel();
		assessmentTable.setFont(new Font("Courrier",Font.PLAIN,12));
		for (int n = 0; n < AssessmentTableModel.NUM_COLUMNS; n++)
			{
			col = new TableColumn(n);
			col.setMinWidth(50);
			col.setHeaderValue(headers[n]);
			colModel.addColumn(col);
			}
		assessmentTable.setColumnModel(colModel);
		}
//============================================================================
//Handles user interaction with the buttons
	public void actionPerformed(ActionEvent e)
		{
		if(e.getSource() == storeButton)
			store();
		else if (e.getSource() == loadButton)
			load();
		else if (e.getSource() == addButton)
			add();
		else if (e.getSource() == editButton)
			edit();
		else if (e.getSource() == deleteButton)
			delete();
		}
//============================================================================
//Stores the values of the JTable in file assessment.dat
	void store()
		{
		try
			{
			dos = new DataOutputStream(new FileOutputStream("assessment.dat"));
			assessmentTableModel.store(dos);
			}
		catch(Exception e)
			{
			JOptionPane.showMessageDialog(this,"Could not save file");
			}
		}
//============================================================================
//Loads the values from file assessment.dat into JTable
	void load()
		{
		try
			{
			dis = new DataInputStream(new FileInputStream("assessment.dat"));
			assessmentTableModel = new AssessmentTableModel(dis);
			assessmentTable.setModel(assessmentTableModel);
			makeColumns();
			}
		catch(Exception e)
			{
			JOptionPane.showMessageDialog(this,"File could not be opened");
			}
		}
//============================================================================
//Creates a dialog to add an assessment
	void add()
		{
		tempDialog = new RecordDialog (assessmentTableModel);
		assessmentTable.repaint();
		}
//============================================================================
//Creates a dialog to edit an assessment
	void edit()
		{
		myAssessment = assessmentTableModel.assessmentModel.elementAt(selectedList[0]);
		tempDialog = new RecordDialog (assessmentTableModel,myAssessment,assessmentTable.getSelectedRow());
		assessmentTable.repaint();
		}
//============================================================================
//Deletes the specified assessment
	void delete()
		{
        for (int n = 0; n < selectedList.length; n++)
        	assessmentTableModel.assessmentModel.removeElementAt((selectedList[n]-n));
        assessmentTable.repaint();
		}
//======================================================================================================
//Keeps track the of the values that are selected by the user
    public void valueChanged(ListSelectionEvent lse)
    	{
        editButton.setEnabled(false);
        deleteButton.setEnabled(false);
        selectedList = assessmentTable.getSelectedRows();
        if(selectedList.length == 1)
        	{
            editButton.setEnabled(true);
            deleteButton.setEnabled(true);
       		}
        else if(selectedList.length > 1)
            deleteButton.setEnabled(true);
    	}
	}