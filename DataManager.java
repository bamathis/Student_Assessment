public interface DataManager
	{
	void addRecord(StudentAssessment studentAssessment);
	void editRecord(StudentAssessment studentAssessment, int index);
	}