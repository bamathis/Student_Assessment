# Student_Assessment

[Student_Assessment](https://gitlab.com/bamathis/Student_Assessment) is a student assessment management program that allows you to record and edit grades for student assignments.

## Quick Start

### Dependencies

- Java 7+

### Program Execution

```
$ java AssessmentFrameClass
```

### Student Assessment Data Storage

Stores/reads student assessments to/from file **assessment.dat** in the current working directory.


